import React from 'react';
import 'bulma/css/bulma.min.css';
import Title from './Title';
import ProfileCards from './ProfileCards';

function App() {
    return (
        <div>
            <Title title="Personal Digital Assistants" />
            <div className="container">
                <ProfileCards />
            </div> 
        </div>
    );
}

export default App;