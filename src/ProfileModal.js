function ProfileModal({handleClose, handleInputChange, handleSubmit}) {
    return(
        <div className="modal" style={{display: 'inline'}}>
            <div className="modal-background"></div>
            <form onSubmit={handleSubmit} method="POST">
                <div className="modal-card" style={{ marginTop: '50px'}}>
                    <header className="modal-card-head">
                        <p className="modal-card-title">Add new profile</p>
                        <button className="delete" aria-label="close"></button>
                    </header>
                    <section className="modal-card-body">
                        <label>
                            Title:
                            <input type="text" name="title" onChange={handleInputChange} />
                        </label>
                        <label>
                            Handle:
                            <input type="text" name="handle" onChange={handleInputChange} />
                        </label>
                        <label>
                            Description:
                            <textarea name="description" onChange={handleInputChange}></textarea>
                        </label>
                    </section>
                    <footer className="modal-card-foot">
                        <button className="button is-success" type="submit">Save</button>
                    </footer>
                </div>
            </form>
            <button className="modal-close is-large" aria-label="close" onClick={handleClose}></button>
        </div>
    );
}

export default ProfileModal;