function Title ({title}) {
    return (
        <section className='hero is-primary'>
            <div className='hero-body'>
                <p className='title'>{ title }</p>
            </div>
        </section>
    );
}

export default Title;