import axios from "axios";

const unsplash_api_link = 'https://api.unsplash.com';

export const generateImage = async () => {
    const response = await axios.get(unsplash_api_link + '/search/photos', {
        headers: {
            Authorization: 'Client-ID mUuQKRWcbXYFkwHYWEMbHKQVict7gWIHzlZYJKsAm1g'
        },
        params: {
            query: 'forest'
        }
    });

    return response.data.results[0].urls.regular;
}