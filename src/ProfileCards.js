import { useState } from 'react';

import ProfileCard from './ProfileCard';
import Button from './Button';
import ProfileModal from './ProfileModal';

import { generateImage } from './api';

const initialValues = {
    title: '',
    handle: '',
    description: '',
    image: ''
}

export default function ProfileCards() {
    const [profiles, setProfiles] = useState([]);
    const [modalState, setModalState] = useState(false);
    const [values, setValues] = useState(initialValues);

    const addProfile = () => {
        setModalState(!modalState);
    };

    const handleInputChange = (event) => {
        const { name, value } = event.target;

        setValues({
            ...values,
            [name]: value,
        });
    };

    const handleFormSubmit = event => {
        event.preventDefault();
        setProfiles([...profiles, values]);
        addProfile();
    };

    return (
        <div>
            <section className='profile-cards section'>
                <div className="profile-cards__list columns">
                    {
                        profiles.length > 0 &&
                            profiles.map((profile, index) => {
                                return (<ProfileCard
                                    key={index}
                                    title={profile.title}
                                    handle={profile.handle}
                                    description={profile.description} 
                                    image='teste'
                                />);
                            })
                    }
                </div>
                <div className='profile-cards__footer buttons'>
                    <Button text="Add" handleClick={addProfile} classes='is-primary' />
                    <Button text="Remove" classes='is-danger' />
                </div>
            </section>
            
            { modalState && 
                <ProfileModal 
                    handleClose={addProfile} 
                    handleInputChange={handleInputChange} 
                    handleSubmit={handleFormSubmit} 
                /> 
            }
        </div>
    );
}